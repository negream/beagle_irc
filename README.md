# Beagle IRC server 

## Installation

### 1. Flash the beaglebone black with debian 9.3 (see <https://beagleboard.org/latest-images>)  
Image file : <http://debian.beagleboard.org/images/bone-debian-9.3-iot-armhf-2018-03-05-4gb.img.xz>

See flashing procedure here : [BBB_burningImage.md](BBB_burningImage.md)

### 2. Enable the serial devices :

Edit *"/boot/uEnv.txt"* and add :

```
###Overide capes with uart
uboot_overlay_addr0=/lib/firmware/BB-UART1-00A0.dtbo
uboot_overlay_addr1=/lib/firmware/BB-UART2-00A0.dtbo
uboot_overlay_addr2=/lib/firmware/BB-UART4-00A0.dtbo
uboot_overlay_addr3=/lib/firmware/BB-UART5-00A0.dtbo
```

after the line *"enable_uboot_overlays=1"*

### 3. Install cmake and boost :

```
sudo apt update
sudo apt install cmake libboost-all-dev
```

### 4. Clone and compile beagle_irc :

```
git clone https://gricad-gitlab.univ-grenoble-alpes.fr/negream/beagle_irc.git
cd beagle_irc
mkdir build
cd build
cmake ..
make
sudo make install
```

### 5. reboot the beaglebone


### More ###

#### Configure Static IP ####

* get interface name :

```
connmanctl services

-> output
*AO Wired                ethernet_xxxxx_cable
```

* configure interface :

```
sudo connmanctl config ethernet_xxxxxxx_cable ipv4 manual 192.168.1.10 255.255.255.0 192.168.1.1
```
  
