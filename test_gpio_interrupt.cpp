//
// Copyright (c) 2018-2019 GIPSA-Lab, CNRS
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
//  * Redistributions of source code must retain the above copyright notice, this
// list of conditions and the following disclaimer.
//  * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
//  * Neither the name of %ORGANIZATION% nor the names of its contributors may be
// used to endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include <boost/asio.hpp>

#include <iostream>
#include <gpio_interrupt.hpp>
#include <boost/bind.hpp>
#include <string>

using namespace std;

//boost::asio::io_service io_service;
//gpio_interrupt interrupt(io_service);
//int i=0;

char buf[2];


class TestInterrupt
{ 
public:
  TestInterrupt(boost::asio::io_service& io)
    : interrupt(io)
      , i(0)
    {
      interrupt.open(27);
      attach_interrupt();
    }

  void attach_interrupt()
    {
      interrupt.async_wait(boost::bind(&TestInterrupt::interrupt_handler, this, _1, _2, _3, 42));  
    }

  void interrupt_handler(const boost::system::error_code& error, unsigned int num, int val, int test)
    {

      cerr << error<< endl;
      /*if (error)
	{
	return;
	}*/

      std::cout<<i++
	<< " " << num 
	<< " " << val
	<< " " << test
	<< endl;

      //interrupt.async_wait(interrupt_handler); 
      attach_interrupt();

      //interrupt.async_read_some(boost::asio::buffer(buf, 2), boost::bind(interrupt_handler, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred)); 
    }

private:
  gpio_interrupt interrupt;
  int i;
};


int main()
{
  boost::asio::io_service io;
  TestInterrupt testInterrupt(io);

  //int fd=::open("/sys/class/gpio/gpio27/value", O_RDONLY);

  //char buf;
  //read(fd, &buf, 1);

  //interrupt.open(27); 
  //interrupt.async_wait(interrupt_handler); 

  //interrupt.async_read_some(boost::asio::buffer(buf, 2), boost::bind(interrupt_handler, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred)); 


  io.run();
  return 0;
}
