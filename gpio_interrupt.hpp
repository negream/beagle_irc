//
// Copyright (c) 2018-2019 GIPSA-Lab, CNRS
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
//  * Redistributions of source code must retain the above copyright notice, this
// list of conditions and the following disclaimer.
//  * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
//  * Neither the name of %ORGANIZATION% nor the names of its contributors may be
// used to endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef GPIO_INTERRUPT_HPP 
#define GPIO_INTERRUPT_HPP 

#include <boost/asio/basic_io_object.hpp> 
#include <boost/asio/ip/udp.hpp> 

class gpio_interrupt_service : public boost::asio::io_service::service { 
private: 
  typedef boost::asio::ip::udp::socket socket_type; 
  typedef socket_type::service_type socket_service_type; 
  typedef socket_type::implementation_type socket_implementation_type; 

public: 
  class implementation_type { 
  private: 
    friend class gpio_interrupt_service; 

    socket_implementation_type underlying_impl_; 
  }; 

  static boost::asio::io_service::id id; 

  gpio_interrupt_service(boost::asio::io_service &io_service); 

  void construct(implementation_type &impl); 

  void destroy(implementation_type &impl); 

  boost::system::error_code open(implementation_type &impl, unsigned int 
number, boost::system::error_code &ec); 

  boost::system::error_code close(implementation_type &impl, 
boost::system::error_code &ec); 

  template<typename Handler> 
  void async_wait(implementation_type &impl, Handler handler) 
  { 
    bound_handler<Handler> bound(*this, impl, handler); 
    socket_service_.async_receive(impl.underlying_impl_, 
boost::asio::null_buffers(), socket_type::message_out_of_band, bound); 
  } 

  boost::system::error_code cancel(implementation_type &impl, 
boost::system::error_code &ec); 

  inline unsigned int get_gpio_num() const
  {
	return gpio_num_;
  }

  inline int get_val() const
  {
	return val_;
  }

private: 
  template<typename Handler> 
  class bound_handler { 
  public: 
    bound_handler(gpio_interrupt_service &service, implementation_type 
&impl, Handler handler) : 
      service_(service), 
      impl_(impl), 
      handler_(std::move(handler)) {} 

    bound_handler(const bound_handler &other) : 
      service_(other.service_), 
      impl_(other.impl_), 
      handler_(other.handler_) {} 

    bound_handler(bound_handler &&other) : 
      service_(other.service_), 
      impl_(other.impl_), 
      handler_(std::move(other.handler_)) {} 

    void operator()(const boost::system::error_code &ec, std::size_t 
bytes_transferred) 
    { 
      service_.reset_descriptor(impl_, ec); 
      handler_(ec,
	service_.get_gpio_num(),
	service_.get_val()); 
    } 

  private: 
    gpio_interrupt_service &service_; 
    implementation_type &impl_; 
    Handler handler_; 
  }; 

  unsigned int gpio_num_;
  int val_;

  socket_service_type &socket_service_; 

  virtual void shutdown_service(); 

  void reset_descriptor(implementation_type &impl, const 
		  boost::system::error_code &ec); 
}; 

class gpio_interrupt : public 
boost::asio::basic_io_object<gpio_interrupt_service> { 
public: 
  gpio_interrupt(boost::asio::io_service &io_service); 

  void open(unsigned int number); 

  boost::system::error_code open(unsigned int number, 
boost::system::error_code &ec); 

  void close(); 

  boost::system::error_code close(boost::system::error_code &ec); 

  template<typename Handler> 
  void async_wait(Handler &&handler) 
  { 
    this->get_service().async_wait(this->get_implementation(), 
std::forward<Handler>(handler)); 
  } 

  void cancel(); 

  boost::system::error_code cancel(boost::system::error_code &ec); 
}; 

#endif // GPIO_INTERRUPT_HPP 
