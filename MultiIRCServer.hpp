//
// Copyright (c) 2018-2019 GIPSA-Lab, CNRS
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
//  * Redistributions of source code must retain the above copyright notice, this
// list of conditions and the following disclaimer.
//  * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
//  * Neither the name of %ORGANIZATION% nor the names of its contributors may be
// used to endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef __MULTI_IRC_SERVER_HPP__
#define __MULTI_IRC_SERVER_HPP__

#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <SerialRC.hpp>
#include <RC_Command.hpp>
#include <map>
#include <stdint.h>
#include <gpio_interrupt.hpp>

#define MAX_PACKET_SIZE 2048

class MultiIRCServer
{
private:
  typedef std::map<uint8_t, SerialRC_Ptr> SerialRCMap_t;
  typedef std::map<uint8_t, boost::shared_ptr<gpio_interrupt> > GPIOMap_t;

  typedef struct StampedRCCommand_t
  {
    RC_Command command;
    boost::posix_time::ptime stamp;
  } StampedRCCommand_t;
  
  typedef std::map<uint8_t, StampedRCCommand_t> CommandMap_t;
  
  SerialRCMap_t serial_rc_map_;
  GPIOMap_t gpio_map_;
  CommandMap_t command_map_;
  
  boost::asio::io_service& io_service_;

  boost::asio::ip::udp::socket socket_;

  boost::array<uint8_t, MAX_PACKET_SIZE> recv_buffer_;

  void start_receive();
  void handle_receive(const boost::system::error_code& error,
		      std::size_t byte_transfered);

  void attach_interrupt(uint8_t id);
  void handle_interrupt(const boost::system::error_code& error,
			unsigned int num,
			int val,
			uint8_t id);

  void parse_message(std::size_t message_size);

  void binding_timer_callback(const boost::system::error_code& error, uint8_t id);

  void loop_timer_callback(const boost::system::error_code& error);
  
  bool synchronous_;
  boost::posix_time::time_duration loop_period_, command_timeout_;
  boost::asio::deadline_timer loop_timer_;
  
  bool binding_;
  uint8_t binding_id_;
  boost::asio::deadline_timer binding_timer_;
  boost::posix_time::time_duration binding_period_;
  RC_Command binding_command_;

public:
  MultiIRCServer(boost::asio::io_service& io_service, unsigned short port=3809);

  void addModule(uint8_t id, const std::string& device, unsigned int gpio);
  
  void setAsynchronous(boost::posix_time::time_duration period, boost::posix_time::time_duration timeout);
  void setSynchronous();

  
  inline void setBindingCommand(const RC_Command& command)
    {
      binding_command_ = command;
    }

  inline void setBindingPeriod(const boost::posix_time::time_duration& period)
    {
      binding_period_ = period;
    }
};

#endif // __MULTI_IRC_SERVER_HPP__
