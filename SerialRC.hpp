//
// Copyright (c) 2018-2019 GIPSA-Lab, CNRS
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
//  * Redistributions of source code must retain the above copyright notice, this
// list of conditions and the following disclaimer.
//  * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
//  * Neither the name of %ORGANIZATION% nor the names of its contributors may be
// used to endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef __SERIAL_RC_HPP__
#define __SERIAL_RC_HPP__

#include <boost/asio.hpp>
#include <boost/asio/serial_port.hpp>
#include <boost/system/system_error.hpp>
#include <RC_Command.hpp>
#include <stdint.h>
#include <string>

typedef boost::shared_ptr<boost::asio::serial_port> serial_port_ptr;


class SerialRC
{
protected:
	serial_port_ptr port_;	
	uint8_t buf[26];

public:
	SerialRC(boost::asio::io_service& io_service, const std::string& device);
	void sendCommand(const RC_Command& command);
	void sendRawCommand(const char* buf, size_t len);
	void sendComplete(const boost::system::error_code& ec,
	                  size_t bytes_transferred);
	
};

typedef boost::shared_ptr<SerialRC> SerialRC_Ptr;

#endif // __SERIAL_RC_HPP__
