//
// Copyright (c) 2018-2019 GIPSA-Lab, CNRS
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
//  * Redistributions of source code must retain the above copyright notice, this
// list of conditions and the following disclaimer.
//  * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
//  * Neither the name of %ORGANIZATION% nor the names of its contributors may be
// used to endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include <MultiIRCServer.hpp>
#include <GPIO/GPIOManager.h>
#include <boost/asio.hpp>
#include <stdint.h>
#include <iostream>

#define GPIO0 61
#define GPIO1 27
#define GPIO2 60
#define GPIO3 115

#define GPIO_ID0 44
#define GPIO_ID1 65
#define GPIO_ID2 46
#define GPIO_ID3 26


using namespace std;

int main(int argc, char **argv)
{

  GPIO::GPIOManager::getInstance()->exportPin(GPIO_ID0);
  GPIO::GPIOManager::getInstance()->exportPin(GPIO_ID1);
  GPIO::GPIOManager::getInstance()->exportPin(GPIO_ID2);
  GPIO::GPIOManager::getInstance()->exportPin(GPIO_ID3);
  GPIO::GPIOManager::getInstance()->setDirection(GPIO_ID0, GPIO::INPUT);
  GPIO::GPIOManager::getInstance()->setDirection(GPIO_ID1, GPIO::INPUT);
  GPIO::GPIOManager::getInstance()->setDirection(GPIO_ID2, GPIO::INPUT);
  GPIO::GPIOManager::getInstance()->setDirection(GPIO_ID3, GPIO::INPUT);

  GPIO::GPIOManager::getInstance()->exportPin(GPIO0);
  GPIO::GPIOManager::getInstance()->exportPin(GPIO1);
  GPIO::GPIOManager::getInstance()->exportPin(GPIO2);
  GPIO::GPIOManager::getInstance()->exportPin(GPIO3);
  GPIO::GPIOManager::getInstance()->setDirection(GPIO0, GPIO::INPUT);
  GPIO::GPIOManager::getInstance()->setDirection(GPIO1, GPIO::INPUT);
  GPIO::GPIOManager::getInstance()->setDirection(GPIO2, GPIO::INPUT);
  GPIO::GPIOManager::getInstance()->setDirection(GPIO3, GPIO::INPUT);
  GPIO::GPIOManager::getInstance()->setEdge(GPIO0, GPIO::BOTH);
  GPIO::GPIOManager::getInstance()->setEdge(GPIO1, GPIO::BOTH);
  GPIO::GPIOManager::getInstance()->setEdge(GPIO2, GPIO::BOTH);
  GPIO::GPIOManager::getInstance()->setEdge(GPIO3, GPIO::BOTH);


  uint8_t base_id = GPIO::GPIOManager::getInstance()->getValue(GPIO_ID0)
    + (GPIO::GPIOManager::getInstance()->getValue(GPIO_ID1)<<1)
    + (GPIO::GPIOManager::getInstance()->getValue(GPIO_ID2)<<2)
    + (GPIO::GPIOManager::getInstance()->getValue(GPIO_ID3)<<3);

  std::cout << "base_id : " << (int) base_id << endl;

  boost::asio::io_service io;

  MultiIRCServer server(io);

  server.addModule(base_id+0, "/dev/ttyS4", GPIO0);
  server.addModule(base_id+1, "/dev/ttyS5", GPIO1);
  server.addModule(base_id+2, "/dev/ttyS1", GPIO2);
  server.addModule(base_id+3, "/dev/ttyS2", GPIO3);

  
  RC_Command binding_command;
  binding_command.key=0x55;
  binding_command.sub_protocol_flag=6|0x80;
  binding_command.rxnum_type_power=0|(0<<4); //8|(0<<4);
  binding_command.option_protocol=7;
  binding_command.data[0]=1000;
  binding_command.data[1]=500;
  binding_command.data[2]=1000;
  binding_command.data[3]=1000;
  binding_command.data[4]=500;
  binding_command.data[5]=500;
  binding_command.data[6]=300;
  binding_command.data[7]=300;

  server.setBindingCommand(binding_command);
  server.setBindingPeriod(boost::posix_time::millisec(22));

  server.setAsynchronous(boost::posix_time::millisec(10), boost::posix_time::seconds(1));
  
  io.run();
  return 0;
}

