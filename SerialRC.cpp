//
// Copyright (c) 2018-2019 GIPSA-Lab, CNRS
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
//  * Redistributions of source code must retain the above copyright notice, this
// list of conditions and the following disclaimer.
//  * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
//  * Neither the name of %ORGANIZATION% nor the names of its contributors may be
// used to endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include <SerialRC.hpp>
#include <boost/bind.hpp>
#include <fcntl.h>
#include <linux/serial.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <iostream>


#define K_NCCS 19
struct termios2 {
        tcflag_t c_iflag;               /* input mode flags */
        tcflag_t c_oflag;               /* output mode flags */
        tcflag_t c_cflag;               /* control mode flags */
        tcflag_t c_lflag;               /* local mode flags */
        cc_t c_line;                    /* line discipline */
        cc_t c_cc[K_NCCS];              /* control characters */
        speed_t c_ispeed;               /* input speed */
        speed_t c_ospeed;               /* output speed */
};

using namespace std;

SerialRC::SerialRC(boost::asio::io_service& io_service, const std::string& device)
{
  boost::system::error_code ec;
  port_ = serial_port_ptr(new boost::asio::serial_port(io_service));
  port_->open(device, ec);
  if(ec)
    {
      std::cerr<<"error : failed to open device " << device << endl
	<< ec.message().c_str() << endl;
      return;
    }

  // set non standart baudrate 100000	
  int handle = port_->native_handle();

  struct termios2 options;
  ioctl(handle, TCGETS2, &options);
  options.c_cflag &= ~CBAUD;    //Remove current BAUD rate
  options.c_cflag |= CBAUDEX;    //Allow custom BAUD rate using int input
  options.c_ispeed = 100000;    //Set the input BAUD rate
  options.c_ospeed = 100000;    //Set the output BAUD rate
  ioctl(handle, TCSETS2, &options);

  //port_->set_option(boost::asio::serial_port_base::baud_rate(115200));
  port_->set_option(boost::asio::serial_port_base::character_size(8));
  port_->set_option(boost::asio::serial_port_base::stop_bits(boost::asio::serial_port_base::stop_bits::two));
  port_->set_option(boost::asio::serial_port_base::parity(boost::asio::serial_port_base::parity::even));
  port_->set_option(boost::asio::serial_port_base::flow_control(boost::asio::serial_port_base::flow_control::none));
}

void SerialRC::sendCommand(const RC_Command& command)
{
  buf[0] = command.key;               //command.sub_protocol<32?0x55:0x54;
  buf[1] = command.sub_protocol_flag; //(command.sub_protocol&0x1f)|(command.bind?0x80:0);
  buf[2] = command.rxnum_type_power;  //command.rx_num | (command.type<<4);
  buf[3] = command.option_protocol;

  const uint16_t * channel_data = command.data;
  uint8_t * packet_data = &buf[4];
  int packet_position;
  for(packet_position = 0; packet_position < 22; packet_position++) packet_data[packet_position] = 0x00;  //Zero out packet data

  int current_packet_bit = 0;
  packet_position = 0;
  for(int current_channel = 0; current_channel < 16; current_channel++)
    {
      //cout << channel_data[current_channel] << " ";
      for(int current_channel_bit = 0; current_channel_bit < 11; current_channel_bit++)
	{
	  if(current_packet_bit > 7)
	    {
	      current_packet_bit = 0;  //If we just set bit 7 in a previous step, reset the packet bit to 0 and
	      packet_position++;       //Move to the next packet byte
	    }
	  packet_data[packet_position] |= (((channel_data[current_channel]>>current_channel_bit) & 0x01)<<current_packet_bit);  //Downshift the channel data bit, then upshift it to set the packet data byte
	  current_packet_bit++;
	}
    }
    //cout << endl;

  sendRawCommand((const char*) buf, 26);
}

void SerialRC::sendRawCommand(const char* buf, size_t len)
{
  boost::asio::async_write(*port_, boost::asio::buffer(buf, len), boost::bind(&SerialRC::sendComplete, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
  /*
     boost::system::error_code ec;
     size_t sent_bytes = 0;
     while(sent_bytes<len)
     sent_bytes += port_->write_some(boost::asio::buffer(buf+sent_bytes, len-sent_bytes), ec);
     */
}

void SerialRC::sendComplete(const boost::system::error_code& ec,
			    size_t bytes_transferred)
{
  if(ec)
    {
      cerr << "failed to send message :" << endl
	<< ec.message().c_str() << endl;
    }else if(bytes_transferred != 26)
      {
	cerr << "message truncated" << endl;
      }
}

