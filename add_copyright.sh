#!/bin/sh
copyright-header --add-path . \
                 --license BSD-3-CLAUSE \
                 --copyright-holder 'GIPSA-Lab, CNRS' \
                 --copyright-software 'Beagle Multi IRC' \
                 --copyright-software-description "Multi protocol RC bridge." \
                 --copyright-year 2018-2019 \
                 --output-dir .
