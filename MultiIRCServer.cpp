//
// Copyright (c) 2018-2019 GIPSA-Lab, CNRS
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
//  * Redistributions of source code must retain the above copyright notice, this
// list of conditions and the following disclaimer.
//  * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
//  * Neither the name of %ORGANIZATION% nor the names of its contributors may be
// used to endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include <MultiIRCServer.hpp>

#include <boost/bind.hpp>
#include <iostream>

using boost::asio::ip::udp;
using namespace std;

MultiIRCServer::MultiIRCServer(boost::asio::io_service& io_service,
                               unsigned short port)
  : io_service_(io_service)
  , socket_(io_service, udp::endpoint(udp::v4(), port))
  , binding_(false)
  , binding_timer_(io_service)
  , loop_timer_(io_service)
  , synchronous_(true)
{
  start_receive();	
}

void MultiIRCServer::addModule(uint8_t id, const std::string& device, unsigned int gpio)
{
  serial_rc_map_[id].reset(new SerialRC(io_service_, device));
  gpio_map_[id].reset(new gpio_interrupt(io_service_));
  gpio_map_[id]->open(gpio);
  attach_interrupt(id);
}

void MultiIRCServer::start_receive()
{
  socket_.async_receive(
			boost::asio::buffer(recv_buffer_),
			boost::bind(&MultiIRCServer::handle_receive, this,
				    boost::asio::placeholders::error,
				    boost::asio::placeholders::bytes_transferred));
}

void MultiIRCServer::handle_receive(const boost::system::error_code& error,
				    std::size_t byte_transfered)
{
  if(!error)
    {
      parse_message(byte_transfered);
    }
  start_receive();
}


void MultiIRCServer::parse_message(std::size_t message_size)
{
  boost::posix_time::ptime stamp = boost::posix_time::microsec_clock::local_time();
  
  std::size_t current_pos=0;
  while(message_size-current_pos >= 37) // 37 bytes per module
  {
    uint8_t id = recv_buffer_[current_pos];
    
    SerialRCMap_t::iterator serial_it = serial_rc_map_.find(id);
    if(serial_it != serial_rc_map_.end())
    {
      if(synchronous_)
      {
        serial_it->second->sendCommand(*(RC_Command*)&recv_buffer_[current_pos+1]);
      
        //serial_it->second->sendRawCommand((const char*)&recv_buffer_[current_pos+1], 26);
      }else{
	//cout << "rcv command [ " << (int) id << "]" << std::endl;
  
	StampedRCCommand_t stamped_command;
        stamped_command.command = *(RC_Command*)&recv_buffer_[current_pos+1];
        stamped_command.stamp = stamp;
          
        command_map_[id] = stamped_command;
      }
      
    }
    current_pos += 37;
  }
}

void MultiIRCServer::attach_interrupt(uint8_t id)
{
  gpio_map_[id]->async_wait(boost::bind(&MultiIRCServer::handle_interrupt, this, _1, _2, _3, id));
}

void MultiIRCServer::handle_interrupt(const boost::system::error_code& error,
				      unsigned int num,
				      int val,
				      uint8_t id)
{
  if(val)
  {
    if(!binding_){
      binding_ = true;
      binding_id_ = id;
      binding_timer_.expires_from_now(binding_period_);
      binding_timer_.async_wait(boost::bind(&MultiIRCServer::binding_timer_callback, this, _1, id));

      binding_command_.sub_protocol_flag |= 0x80; // set bind flag
      serial_rc_map_[id]->sendCommand(binding_command_);
      binding_command_.sub_protocol_flag &= ~(0x80); // unset bind flag

      cout << "Start BINDING Module " << (int) id << endl;
    }

  }else{
    if(id==binding_id_){
      binding_timer_.cancel();
      binding_ = false;
      cout << "End BINDING" << endl;
    }
  }

  attach_interrupt(id);
}


void MultiIRCServer::binding_timer_callback(const boost::system::error_code& error, uint8_t id)
{
  if(error)
    return;

  serial_rc_map_[id]->sendCommand(binding_command_);

  binding_timer_.expires_at(binding_timer_.expires_at()+binding_period_);
  binding_timer_.async_wait(boost::bind(&MultiIRCServer::binding_timer_callback, this, _1, id));

  //cout << ".";
  //cout.flush();
}

void MultiIRCServer::loop_timer_callback(const boost::system::error_code& error)
{
  if(error)
    return;
  
  for(CommandMap_t::iterator it = command_map_.begin();
      it !=command_map_.end();
      ++it)
  {
    if(loop_timer_.expires_at()-it->second.stamp < command_timeout_)
    {
      SerialRCMap_t::iterator serial_it = serial_rc_map_.find(it->first);
      if(serial_it != serial_rc_map_.end())
      {
        serial_it->second->sendCommand(it->second.command);
      }
    }
  }
  loop_timer_.expires_at(loop_timer_.expires_at()+loop_period_);
  loop_timer_.async_wait(boost::bind(&MultiIRCServer::loop_timer_callback, this, _1));
}

void MultiIRCServer::setAsynchronous(boost::posix_time::time_duration period,
                                     boost::posix_time::time_duration timeout)
{
  loop_period_ = period;
  command_timeout_ = timeout;
  
  loop_timer_.expires_from_now(loop_period_);
  loop_timer_.async_wait(boost::bind(&MultiIRCServer::loop_timer_callback, this, _1));
  
  synchronous_ = false;
}

void MultiIRCServer::setSynchronous()
{
  synchronous_ = true;
}
