#!/usr/bin/pythonimport argparse

# Copyright (c) 2018-2019 GIPSA-Lab, CNRS
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#
#  * Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation and/or
# other materials provided with the distribution.
#  * Neither the name of %ORGANIZATION% nor the names of its contributors may be
# used to endorse or promote products derived from this software without specific
# prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import socket
import struct
import click

from threading import Timer
import time

import sys

import os

parser = argparse.ArgumentParser(description='UDP irc client.')
parser.add_argument('host', default='192.168.7.2', nargs='?')
parser.add_argument('port', default=3809, type=int, nargs='?')

args = parser.parse_args()


sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)


def build_packet(channels):
    data = ''
    for i in range(4):
        data = data + struct.pack('<5B16H', i, 0x55, 0x06, 0x28, 0, *channels)
    return data



channels=[1024, 1024, 200, 1024, 200, 200, 200, 200, 0, 0, 0, 0, 0, 0, 0, 0]
packet = build_packet(channels)

print(repr(packet))

def sendPacket():
    sock.sendto(packet, (args.host, args.port))


def loop(period):
    #print('coucou')
    sendPacket()
    Timer(period, loop, [period]).start()


#try:
print(args.host)
print(args.port)
loop(0.011)
        
c=''
while c!=u'\x1b' and c!=u'q':
    c = click.getchar()
    if c==u'\x1b[A':
        channels[0]=1024
        channels[1]=1024
        channels[2]=1000
        channels[3]=1024
        packet = build_packet(channels)
        print('up')
    elif c==u'\x1b[B':
        channels[0]=1024
        channels[1]=1024
        channels[2]=200
        channels[3]=1024
        packet = build_packet(channels)
        print('bottom')
    elif c==u'\x1b[D':
        #channels[0]=1000
        #build_packet(channels)
       print('left')
    elif c==u'\x1b[C':
        #channels[1]=1000
        #build_packet(channels)
        print('right')
    else:
        print(repr(c))
#except:
#    print('exit')
#    sys.exit(0)

os._exit(0)
