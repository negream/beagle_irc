//
// Copyright (c) 2018-2019 GIPSA-Lab, CNRS
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
//  * Redistributions of source code must retain the above copyright notice, this
// list of conditions and the following disclaimer.
//  * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
//  * Neither the name of %ORGANIZATION% nor the names of its contributors may be
// used to endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include <SerialRC.hpp>
#include <boost/bind.hpp>
#include <boost/thread.hpp> 
#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include <GPIO/GPIOManager.h>

#include <stdint.h>
#include <iostream>

#define GPIO1 61
#define GPIO2 27
#define GPIO3 60
#define GPIO4 115

using namespace std;

boost::shared_ptr<SerialRC> rc0;
boost::shared_ptr<boost::asio::deadline_timer> timer, timer2;
boost::posix_time::milliseconds period(5);
RC_Command command;


int prev_v = GPIO::LOW;

void timer2Callback(const boost::system::error_code& /*e*/)
{
	GPIO::GPIOManager::getInstance()->setValue(45, GPIO::LOW);
}

void timerCallback(const boost::system::error_code& /*e*/)
{
	//cout << "timer callback" << endl;

	int v1 = GPIO::GPIOManager::getInstance()->getValue(GPIO1);
	int v3 = GPIO::GPIOManager::getInstance()->getValue(GPIO3);

	command.bind = v3?true:false;
	command.data[2] = v1?900:200;

	//prev_v = v;
	rc0->sendCommand(command);

	timer->expires_at(timer->expires_at() + period);
	timer->async_wait(&timerCallback);
	
	//timer2->expires_at(timer->expires_at() + boost::posix_time::milliseconds(1));
	//timer2->async_wait(&timer2Callback);
}

int main(int argc, char **argv)
{
	boost::asio::io_service io;
	
	rc0 = boost::shared_ptr<SerialRC>(new SerialRC("/dev/ttyS1", io));
	
	GPIO::GPIOManager::getInstance()->exportPin(GPIO1);
	GPIO::GPIOManager::getInstance()->setDirection(GPIO1, GPIO::INPUT);
	
	GPIO::GPIOManager::getInstance()->exportPin(GPIO2);
	GPIO::GPIOManager::getInstance()->setDirection(GPIO2, GPIO::INPUT);
	
	GPIO::GPIOManager::getInstance()->exportPin(GPIO3);
	GPIO::GPIOManager::getInstance()->setDirection(GPIO3, GPIO::INPUT);

	GPIO::GPIOManager::getInstance()->exportPin(GPIO4);
	GPIO::GPIOManager::getInstance()->setDirection(GPIO4, GPIO::INPUT);


	init_DSMX2_11_Command(command, false);
	command.data[0] = 500;
	command.data[1] = 500;
	command.data[2] = 500;
	command.data[3] = 500;
	command.data[4] = 500;
	command.data[5] = 500;
	command.data[6] = 0;
	command.data[7] = 0;
	
	timer = boost::shared_ptr<boost::asio::deadline_timer>(new boost::asio::deadline_timer(io, period));
	timer->async_wait(&timerCallback);
	
	timer2 = boost::shared_ptr<boost::asio::deadline_timer>(new boost::asio::deadline_timer(io, period));
	io.run();

	return 0;
}


