//
// Copyright (c) 2018-2019 GIPSA-Lab, CNRS
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
//  * Redistributions of source code must retain the above copyright notice, this
// list of conditions and the following disclaimer.
//  * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
//  * Neither the name of %ORGANIZATION% nor the names of its contributors may be
// used to endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef __RC_COMMAND_HPP__
#define __RC_COMMAND_HPP__

#include <stdint.h>

struct RC_Command
{
	uint8_t key; // 0x55, 0x54 for channels ; 0x57 or 0x56 for failsafe
	uint8_t sub_protocol_flag; // sub_protocol (5bits) and flags (bind, rangecheck, autobind)
	uint8_t rxnum_type_power; // rxnum (4bits), type(3bits) power
	uint8_t option_protocol; // protocol additional option
	
	//uint16_t sub_protocol;
	//bool bind;
	//uint8_t rx_num;
	//uint8_t type;
	//int8_t option_protocol;
	uint16_t data[16];

	inline void setBindBit()
	{
		sub_protocol_flag |= 0x80;
	}
	inline void clearBindBit()
	{
		sub_protocol_flag &= ~(0x80);
	}
};

/*inline void init_DSMX2_22_Command(RC_Command& command, bool bind=false)
{
	command.sub_protocol=6;
	command.bind = bind;
	command.rx_num = 8;
	command.type = 0;
	command.option_protocol = 0;
	for(int i=0; i<16; i++)
	{
		command.data[i] = 0;
	}
}

inline void init_DSMX2_11_Command(RC_Command& command, bool bind=false)
{
	command.sub_protocol=6;
	command.bind = bind;
	command.rx_num = 8;
	command.type = 1;
	command.option_protocol = 0;
	for(int i=0; i<16; i++)
	{
		command.data[i] = 0;
	}
}
*/

#endif //__RC_COMMAND_HPP__
