#BeagleBone Black: Burning an OS image to a microSD card and installing

Your BBB may already come with an OS installed. Check this. If this is good enough for you, you're done.

We are currently installing Debian 9.3 

Check to see whether we already have a current installation image on a MicroSD card for you to use. If not, burn your own. Note that this can take up to an hour of your time or more if things don't go as planned.

## Procedure

* Download the image onto your host computer from http://beagleboard.org/latest-images
  
* Extract the archive :
    
    xz -d xxxxxxxx.img.xz
  
* Write image to microSD. Here's how:

on Mac: 
Insert SD card and execute the following terminal commands:

To see a list of connected storage devices:

    df -h
  
Run this command twice and compare to find the microSD device. When I did this, my SD card was /dev/disk2s1.

Unmount the disk (use your disk details in place of mine):

    sudo diskutil unmount /dev/disk2s1

And enter your computer's password because sudo.

Execute this command carefully. Don't try to copy over the wrong directory, like your 500G hard drive.
Note that we change the name of the SD disk a little bit by ignoring the partition code. Double czech it!:

    sudo dd if=~/path/to.img of=/dev/disknumber oflag=direct conv=fsync bs=4M status=progress
  
So for me this is:

    sudo dd if=bone-debian-9.3-iot-armhf-2018-03-05-4gb.img of=/dev/sdc oflag=direct conv=fsync bs=4M status=progress
  
Enter your password and wait. The output in terminal will be something like:

    3400+0 records in
    3400+0 records out
    3565158400 bytes transferred in 73.138812 secs (48745096 bytes/sec)

If you get an error like "The disk you inserted was not readable by this computer', ignore it. It's just the Mac freaking out because there's a Linux partition on the sd card. Instead, go back to the command line and:

    sudo diskutil eject /dev/sdc

or whatever number your disk is.


* Flash the BeagleBone eMMC

UNPLUG EVERYTHING INCLUDING POWER FROM THE BEAGLEBONE

Insert the microSD card

Hold down the Boot button (the isolated button located on the other side of the SD card reader). Keep holding it down while plugging in power and until you see all four LEDs on together. Then let go and wait for the BeagleBone to boot off the card. The heartbeat LED will start up when it is finished. Ssh in via ethernet: 

    ssh debian@beaglebone.local
    # password temppwd
    sudo nano /boot/uEnv.txt
    
and uncomment this line (it may be the last line in the list):

    cmdline=init=/opt/scripts/tools/eMMC/init-eMMC-flasher-v3.sh
    
This will allow the disk image to flash the eMMC. So now you can go do that.

Shutdown the BeagleBone either through

    sudo shutdown now
    
or by pressing the shutdown button on the BeagleBone.

Do the same startup procedure. Unplug everything, press the boot button while plugging in the power, release the boot button once all four leds come on. Watch the pretty Knight Rider led sequence. Then pull yourself away because it is going to do that for about half an hour. When it shuts down on its own, it is finished.

You should have a nice clean, new install. Now you can use the Connman Cheat Sheet gist to set up your wireless Internet connection. Then you can SET UP A SCREEN SESSION and run:

    sudo apt-get update
    sudo apt-get upgrade
    
to get any OS and package updates shoud you so choose.










  
