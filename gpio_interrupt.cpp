//
// Copyright (c) 2018-2019 GIPSA-Lab, CNRS
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
//  * Redistributions of source code must retain the above copyright notice, this
// list of conditions and the following disclaimer.
//  * Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
//  * Neither the name of %ORGANIZATION% nor the names of its contributors may be
// used to endorse or promote products derived from this software without specific
// prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "gpio_interrupt.hpp" 

#include <boost/asio/detail/throw_error.hpp> 
#include <boost/asio/error.hpp> 

#include <fcntl.h> 

boost::asio::io_service::id gpio_interrupt_service::id; 

gpio_interrupt_service::gpio_interrupt_service(boost::asio::io_service &io_service) : 
  service(io_service), 
  socket_service_(boost::asio::use_service<socket_service_type>(io_service)) 
{ 
} 

void gpio_interrupt_service::construct(implementation_type &impl) 
{ 
  socket_service_.construct(impl.underlying_impl_); 
} 

void gpio_interrupt_service::destroy(implementation_type &impl) 
{ 
  socket_service_.destroy(impl.underlying_impl_); 
} 

boost::system::error_code 
gpio_interrupt_service::open(implementation_type &impl,
			     unsigned int number,
			     boost::system::error_code &ec) 
{ 
  std::ostringstream filename; 
  filename << "/sys/class/gpio/gpio" << number << "/value"; 

  int fd = ::open(filename.str().c_str(), O_RDONLY | O_NONBLOCK); 
  if (fd < 0) { 
      ec = boost::system::error_code(errno, 
				     boost::asio::error::get_system_category()); 
      return ec; 
  } 

  gpio_num_ = number;

  reset_descriptor(impl, ec); 
  return socket_service_.assign(impl.underlying_impl_, 
				boost::asio::ip::udp::v4(), fd, ec); 
} 

boost::system::error_code 
gpio_interrupt_service::close(implementation_type &impl, 
			      boost::system::error_code &ec) 
{ 
  return socket_service_.close(impl.underlying_impl_, ec); 
} 

boost::system::error_code 
gpio_interrupt_service::cancel(implementation_type &impl, 
			       boost::system::error_code &ec) 
{ 
  return socket_service_.cancel(impl.underlying_impl_, ec); 
} 

void gpio_interrupt_service::shutdown_service() 
{ 
} 

void gpio_interrupt_service::reset_descriptor(implementation_type &impl, 
					      const boost::system::error_code &ec) 
{ 
  if (ec) 
    return; 

  int fd = socket_service_.native_handle(impl.underlying_impl_); 
  lseek(fd, 0, SEEK_SET); 
  char buf[2]; 
  read(fd, buf, 2);

  val_ = atoi(buf);
} 

gpio_interrupt::gpio_interrupt(boost::asio::io_service &io_service) : 
  basic_io_object(io_service) 
{ 
} 

void gpio_interrupt::open(unsigned int number) 
{ 
  boost::system::error_code ec; 
  this->get_service().open(this->get_implementation(), number, ec); 
  boost::asio::detail::throw_error(ec, "open"); 
} 

boost::system::error_code gpio_interrupt::open(unsigned int number, 
					       boost::system::error_code &ec) 
{ 
  return this->get_service().open(this->get_implementation(), number, 
				  ec); 
} 

void gpio_interrupt::close() 
{ 
  boost::system::error_code ec; 
  this->get_service().close(this->get_implementation(), ec); 
  boost::asio::detail::throw_error(ec, "close"); 
} 

boost::system::error_code 
gpio_interrupt::close(boost::system::error_code &ec) 
{ 
  return this->get_service().close(this->get_implementation(), ec); 
} 

void gpio_interrupt::cancel() 
{ 
  boost::system::error_code ec; 
  this->get_service().cancel(this->get_implementation(), ec); 
  boost::asio::detail::throw_error(ec, "cancel"); 
} 

boost::system::error_code 
gpio_interrupt::cancel(boost::system::error_code &ec) 
{ 
  return this->get_service().cancel(this->get_implementation(), ec); 
} 
